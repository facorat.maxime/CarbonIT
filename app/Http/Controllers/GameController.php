<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidInitFileException;
use App\Exceptions\InvalidOutputFileException;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /**
     * @param string $path
     * @return array|null
     * @throws InvalidInitFileException
     */
    public function readInitFile(string $path)
    {
        if(empty($path))
        {
            throw new InvalidInitFileException("Entry path is emtpy", 500);
        }
        $result = [];
        $handle = fopen($path, "r");
        if ($handle) {
            while (($line = (fgets($handle))) !== false) {
                $line = preg_replace("/\r|\n/", "", $line);
                array_push($result, $line);
            }
            fclose($handle);
        } else {
            throw new InvalidInitFileException("An error occurred while reading input file", 500);
        }
        return $result;
    }

    /**
     * @param $path
     * @param $text
     * @return array|bool
     * @throws InvalidOutputFileException
     */
    public function writeOutputFile($path, $text)
    {
        $result = [];
        $handle = fopen($path, "w");
        if ($handle) {
            file_put_contents($path, $text);
        } else {
            return false;
        }

        fclose($handle);

        $handle = fopen($path, "r");
        if ($handle) {
            while (($line = (fgets($handle))) !== false) {
                $line = preg_replace("/\r|\n/", "", $line);
                array_push($result, $line);
            }
            fclose($handle);
        } else {
            throw new InvalidOutputFileException("An error occurred while reading input file", 500);
        }
        return $result;
    }

}
