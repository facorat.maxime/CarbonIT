<?php

namespace App\Http\Controllers;

use App\Exceptions\EmptyMapException;
use App\Exceptions\InvalidInitFileException;
use App\Exceptions\InvalidOutputFileException;
use App\Map;

/**
 * Class IndexController
 * @package App\Http\Controllers
 */
class IndexController extends Controller
{
    const WRONG_INPUT_PATH = __DIR__;
    const WRONG_INPUT_FILE =  __DIR__ . "/../../../input/fake.txt";
    const INPUT_PATH = __DIR__ . "/../../../input/input.txt";
    const OUTPUT_PATH = __DIR__ . "/../../../input/output.txt";

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        try {
        $game = new GameController();
        $input = $game->readInitFile($this::INPUT_PATH);
        $map = new Map($input);
        $outputMap = $this->printOutputMap($map);
        $players = $map->getPlayers();
        $output = $this->writeOutputFile($this::OUTPUT_PATH, $map, $input);

        } catch (\Exception $e)
        {
            abort(500);
        }
        return view('index', compact('map', 'input', 'outputMap', 'output', 'players'));
    }

    /**
     * @param Map $map
     * @return bool
     * @throws EmptyMapException
     */
    public function printOutputMap($map)
    {
        $text = $map->showMap();

        if(empty($text))
        {
            throw new EmptyMapException("Map could not be shown", 500);
        }
        return $text;
    }

    /**
     * @param $path
     * @param Map $map
     * @param $input
     * @return bool|string
     * @throws \App\Exceptions\InvalidOutputFileException
     * @throws InvalidInitFileException
     */
    public function writeOutputFile($path, $map, $input)
    {
        if(empty($input))
        {
            throw new InvalidInitFileException("Output file could not been rendered", 500);
        }

        $game = new GameController();
        $text = "";
        foreach ($input as $lines) {
            switch ($lines[0]) {
                case "C":
                case "M":
                    $text .= $lines . "\n";
                    break;
                default:
                    break;

            }
        }

        for ($row = 0; $row < $map->getHeight(); $row++) {
            for ($col = 0; $col < $map->getWidth(); $col++) {
                $key = $map->getMap()[$col][$row]["key"];
                $value = $map->getMap()[$col][$row]["value"];
                if ($key == Map::TREASURE_CHAR) {
                    $text .= "T - " . $col . " - " . $row . " - " . $value . "\n";
                }
                if ($key == Map::PLAYER_CHAR) {
                    $text .= "A - " . $col . " - " . $row . "\n";
                }
            }
        }
        $text = explode("\n", $text);

        if(empty($text))
        {
            throw new InvalidOutputFileException("Output file could not been rendered", 500);
        }

        $output = $game->writeOutputFile($path, $text);

        if(empty($output))
        {
            throw new InvalidOutputFileException("Output file could not been rendered", 500);
        }

        return $output;
    }
}

