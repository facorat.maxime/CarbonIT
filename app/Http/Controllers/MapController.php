<?php

namespace App\Http\Controllers;

use App\Exceptions\ConflictingClusterException;
use App\Exceptions\InvalidInitFileException;
use App\Map;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class MapController extends Controller
{
    public function create(Request $request)
    {
        $map = [];

        $file = $request->get('file');
        $game = new GameController();
        try {
            $input = $game->readInitFile($file);
            $map = new Map($input);
        } catch (\Exception $e) {
            return new Response(null, 500);
        }

        return new Response($map, 200);
    }

    public function show(Request $request)
    {
        $map = [];

        $file = $request->get('file');
        $game = new GameController();
        try {
            $input = $game->readInitFile($file);
            $map = new Map($input);
        } catch (\Exception $e) {
            return new Response(null, 500);
        }
        if (empty($map)) {
            return new Response(null, 500);
        }

        return new Response(json_encode($map->showMap()), 200);
    }

    /**
     * @return Map|array
     */
    public function MapForTesting()
    {
        try {
            $game = new GameController();
            $input = $game->readInitFile(IndexController::INPUT_PATH);
            $map = new Map($input);
        } catch (Exception $e) {
            return [];
        }
        return $map;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \App\Exceptions\OutOfMapRangeException
     */
    public function checkMapSize(Request $request)
    {
        $col = $request->get('col');
        $row = $request->get('row');
        $m = $this->MapForTesting();
        $check = $m->checkMapSize($col, $row);
        if ($check) {
            return new Response("True", 200);
        } else {
            return new Response("False", 500);
        }
    }


    /**
     * @param $col
     * @param $row
     * @return Response
     * @throws \App\Exceptions\EmptyMapException
     */
    public function checkMapInstance()
    {
        $m = $this->MapForTesting();
        $check = $m->checkMapInstance();
        if ($check) {
            return new Response("True", 200);
        } else {
            return new Response("False", 500);
        }
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \App\Exceptions\InitMapException
     */
    public function checkMapInitialization(Request $request)
    {
        $col = $request->get('col');
        $row = $request->get('row');
        $m = $this->MapForTesting();
        $check = $m->checkMapInitialization($col, $row);
        if ($check) {
            return new Response("True", 200);
        } else {
            return new Response("False", 500);
        }
    }


    /**
     * @param Request $request
     * @return ConflictingClusterException|Response
     */
    public function checkFreeCluster(Request $request)
    {
        $col = $request->get('col');
        $row = $request->get('row');
        $m = $this->MapForTesting();
        $check = $m->isFreeCluster($col, $row);
        if ($check) {
            return new Response("True", 200);
        } else {
            return new Response("False", 500);
        }
    }

}
