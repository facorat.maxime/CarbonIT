<?php
/**
 * Created by PhpStorm.
 * User: Maxime Facorat
 * Date: 31/03/2018
 * Time: 10:45
 */

namespace App\Exceptions;


use Exception;

class InvalidInitFileException extends Exception
{

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }


    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }


    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @param mixed $line
     */
    public function setLine($line)
    {
        $this->line = $line;
    }


}