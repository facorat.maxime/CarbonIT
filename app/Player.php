<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    /**
     * @return null
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param null $score
     */
    public function setScore($score)
    {
        $this->score = $score;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isWinner(): bool
    {
        return $this->isWinner;
    }

    /**
     * @param bool $isWinner
     */
    public function setIsWinner(bool $isWinner)
    {
        $this->isWinner = $isWinner;
    }

    /**
     * @return null
     */
    public function getHeading()
    {
        return $this->heading;
    }

    /**
     * @param null $heading
     */
    public function setHeading($heading)
    {
        $this->heading = $heading;
    }

    /**
     * @return null
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param null $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    protected $score;
    protected $name;
    protected $isWinner;
    protected $heading;
    protected $query;
    protected $col;
    protected $row;

    /**
     * Player constructor.
     * @param $values
     */
    public function __construct($values)
    {
        $this->score = (isset($values['score'])) ? $values['score'] : 0;
        $this->name = (isset($values['name'])) ? $values['name'] : null;
        $this->isWinner = false;
        $this->heading = (isset($values['heading'])) ? $values['heading'] : null;
        $this->query = (isset($values['query'])) ? $values['query'] : null;
        $this->col = (isset($values['col'])) ? $values['col'] : null;
        $this->row = (isset($values['row'])) ? $values['row'] : null;
    }
}
