<?php

namespace App;

use App\Exceptions\ConflictingClusterException;
use App\Exceptions\EmptyMapException;
use App\Exceptions\InitCharacterException;
use App\Exceptions\InitMapException;
use App\Exceptions\InvalidInitFileException;
use App\Exceptions\OutOfMapRangeException;
use Exception;
use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
    protected $height;
    protected $width;
    protected $map = [];
    protected $players = [];


    const FREE_CHAR = " - ";
    const MOUNTAIN_CHAR = "M";
    const TREASURE_CHAR = "T";
    const PLAYER_CHAR = "A";

    /**
     * Map constructor.
     * @param $file
     * @throws Exception
     */
    public function __construct($file)
    {
        parent::__construct();

        $this->checkFile($file);
        $map = [];
        foreach ($file as $line) {
            switch ($line[0]) {
                case "C":
                    if (!empty($this->getMap())) {
                        throw new InitMapException("Map already initialized", 500);
                    }
                    $content = explode(" ", $line);
                    $this->checkFile($content);
                    $width = $content[2];
                    $height = $content[4];
                    $map = [];

                    // FILL OUT MAP WITH EMPTY VALUE
                    for ($row = 0; $row < $height; $row++) {
                        for ($col = 0; $col < $width; $col++) {
                            $map[$col][$row] = [
                                "col" => $col,
                                "row" => $row,
                                "key" => Map::FREE_CHAR,
                                "value" => " "
                            ];
                        }
                    }

                    $this->setHeight($height);
                    $this->setWidth($width);
                    $this->setMap($map);
                    $this->checkMapInitialization($width, $height);
                    break;

                case "M":
                    $this->checkMapInstance();

                    $content = explode(" ", $line);
                    $col = $content[2];
                    $row = $content[4];
                    $this->checkMapSize($col, $row);
                    if ($this->isFreeCluster($col, $row)) {
                        $map[$col][$row]["key"] = Map::MOUNTAIN_CHAR;
                        $map[$col][$row]["value"] = "#";
                    }
                    break;

                case "T":
                    $this->checkMapInstance();

                    $content = explode(" ", $line);
                    $col = $content[2];
                    $row = $content[4];
                    $this->checkMapSize($col, $row);
                    $number = $content[6];
                    if ($this->isFreeCluster($col, $row)) {
                        $map[$col][$row]["key"] = Map::TREASURE_CHAR;
                        $map[$col][$row]["value"] = $number;
                    }
                    break;

                case "A":
                    $this->checkMapInstance();

                    $content = explode(" ", $line);
                    $col = $content[4];
                    $row = $content[6];
                    $this->checkMapSize($col, $row);
                    $name = $content[2];
                    if ($this->isFreeCluster($col, $row)) {
                        $map[$col][$row]["key"] = Map::PLAYER_CHAR;
                        $map[$col][$row]["value"] = $name;
                    }

                    $character = ["name" => $name, "heading" => $content[8], "query" => $content[10], "col" => $col, "row" => $row];
                    $this->addPlayer($character);
                    break;
                case "#":
                    break;
            }
            $this->setMap($map);
        }

        $this->setMap($map);
        return $map;
    }

    /**
     * @return array
     */
    public function getPlayers(): array
    {
        return $this->players;
    }

    public function addPlayer($values)
    {
        $p = new Player($values);
        array_push($this->players, $p);
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     * @return int
     */
    public function setHeight($height): int
    {
        $this->height = $height;
        return $height;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     * @return int
     */
    public function setWidth($width): int
    {
        $this->width = $width;
        return $width;
    }

    /**
     * @return array
     */
    public function getMap(): array
    {
        return $this->map;
    }

    /**
     * @param array $map
     * @return array
     */
    public function setMap(array $map): array
    {
        $this->map = $map;
        return $map;
    }

    /**
     * @return bool
     */
    public function printMapRaw()
    {
        echo "<table id='resultTable' class=\"dataTable display cell-border compact hover order-column row-border stripe\"><thead><tr><th>#</th>";
        for ($i = 0; $i > $this->getWidth(); $i++) {
            echo "<th>C" . $i . "</th>";
        }
        echo "</tr></thead><tbody>";
        for ($row = 0; $row < $this->getHeight(); $row++) {
            echo "<tr>";
            echo "<td>R" . $row . "</td>";
            for ($col = 0; $col < $this->getWidth(); $col++) {
                echo "<td>";
                $key = $this->getMap()[$col][$row]["key"];
                $value = $this->getMap()[$col][$row]["value"];
                if (!empty($key) && !is_null($key)) {
                    if ($key == "A" || $key == "T") {
                        echo(" " . $key . " (" . $value . ") ");
                    } else {
                        echo(" " . $key . " ");
                    }
                } else {
                    echo " ";
                }
                echo "</td>";
            }
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
        return true;
    }

    /**
     * @return string
     */
    public function showMap()
    {
        $table = "";
        $table .= "<table id='resultTable' class=\"dataTable display cell-border compact hover order-column row-border stripe\"><thead><tr><th>#</th>";
        for ($i = 0; $i < $this->getWidth(); $i++) {
            $table .= "<th>C" . $i . "</th>";
        }
        $table .= "</tr></thead><tbody>";
        for ($row = 0; $row < $this->getHeight(); $row++) {
            $table .= "<tr>";
            $table .= "<td>R" . $row . "</td>";
            for ($col = 0; $col < $this->getWidth(); $col++) {
                $table .= "<td>";
                $key = $this->getMap()[$col][$row]["key"];
                $value = $this->getMap()[$col][$row]["value"];
                if (!empty($key) && !is_null($key)) {
                    if ($key == "A" || $key == "T") {
                        $table .= " " . $key . " (" . $value . ") ";
                    } else {
                        $table .= " " . $key . " ";
                    }
                } else {
                    $table .= " ";
                }
                $table .= "</td>";
            }
            $table .= "</tr>";
        }
        $table .= "</tbody>";
        $table .= "</table>";
        return $table;
    }

    /**
     * @param $col
     * @param $row
     * @return bool
     * @throws OutOfMapRangeException
     */
    public function checkMapSize($col, $row)
    {
        if ($col > $this->getWidth()) {
            throw new OutOfMapRangeException("Horizontal value is out of map scope", 500);
        }
        if ($row > $this->getHeight()) {
            throw new OutOfMapRangeException("Vertical value is out of map scope", 500);
        }

        return true;
    }

    /**
     * @throws EmptyMapException
     */
    public function checkMapInstance()
    {
        if (empty($this->getMap())) {
            throw new EmptyMapException("Map is empty, game could not continue", 500);
        }

        return true;
    }

    /**
     * @param $col
     * @param $row
     * @return bool
     * @throws InitMapException
     */
    public function checkMapInitialization($col, $row)
    {
        if (is_null($col) || !is_numeric($col) || is_null($row) || !is_numeric($row)) {
            throw new InitMapException("Columns or rows values given are not allowed");
        }

        return true;
    }

    /**
     * @param $content
     * @return bool
     * @throws InvalidInitFileException
     */
    public function checkFile($content)
    {
        if (is_null($content) || empty($content)) {
            throw new InvalidInitFileException("Content for initializing map is empty");
        }

        return true;
    }

    /**
     * @param $col
     * @param $row
     * @return ConflictingClusterException|bool
     */
    public function isFreeCluster($col, $row)
    {
        if (empty($this->getMap()[$col][$row]["key"]) || $this->getMap()[$col][$row]["key"] == Map::FREE_CHAR) {
            return true;
        } else {
            return new ConflictingClusterException("Cluster already used");
        }
    }
}

