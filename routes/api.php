<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/test/read-input-file/{path}', 'GameController@readInitFile');
Route::get('/test/write-output-file', 'GameController@writeOutputFile');
Route::get('/map/init', 'MapController@index');
Route::get('/map/show', 'MapController@show');
Route::get('/map/check/map-size', 'MapController@CheckMapSizeTest');
Route::get('/map/check/map-instance', 'MapController@CheckMapInstanceTest');
Route::get('/map/check/map-init', 'MapController@CheckMapInitializationTest');
Route::get('/map/check/map-input-file', 'MapController@CheckFileTest');
Route::get('/map/check/map-free-cluster', 'MapController@CheckFreeClusterTest');