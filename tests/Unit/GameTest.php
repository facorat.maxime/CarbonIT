<?php
/**
 * Created by PhpStorm.
 * User: Maxime Facorat
 * Date: 04/04/2018
 * Time: 09:10
 */

namespace Tests\Unit;


use Tests\TestCase;

class GameTest extends TestCase
{
    /**
     * @test
     */
    public function readInitFileTest()
    {
        $path = __DIR__ . "/../../input/input.txt";

        $response = $this->get('/test/read-input-file/' . $path);
        $response->assertStatus(200);

        $path = "fakePath";

        $response = $this->get('/test/read-input-file/' . $path);
        $response->assertStatus(500);
    }

}