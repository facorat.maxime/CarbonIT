<?php

namespace Tests\Unit;

use App\Map;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IndexTest extends TestCase
{
    /**
     * @test
     *
     * @return void
     */
    public function IndexTest()
    {
        $request = $this->get('/');
        $request->assertStatus(200);
    }

    public function PrintOutputMapTest()
    {
        try {
            $map = new Map(null);
        } catch (\Exception $e) {
            return false;
        }

        $request = $this->get('/map/print');
        $request->assertStatus(500);
    }

}
