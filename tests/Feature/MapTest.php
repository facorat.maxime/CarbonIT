<?php

namespace Tests\Feature;

use App\Http\Controllers\GameController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\MapController;
use App\Map;
use Exception;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MapTest extends TestCase
{



    /**
     * A basic test example.
     * @test
     * @return void
     */
    public function MapConstructorTest()
    {
        $request = $this->get('api/map/init?file=/mnt/c/www/carbonit/input/input.txt');
        $request->assertStatus(200);

        $request = $this->get('api/map/init?file=test');
        $request->assertStatus(500);
    }

    /**
     * @test
     */
    public function MapPrintTest()
    {
        $request = $this->get('api/map/show?file=/mnt/c/www/carbonit/input/input.txt');
        $request->assertStatus(200);

        $request = $this->get('api/map/show?file=test');
        $request->assertStatus(500);
    }

    /**
     * @test
     */
    public function CheckMapSizeTest()
    {
        $request = $this->get('/map/check/map-size?col=3&row=3');
        $request->assertStatus(200);

        $request = $this->get('/map/check/map-size?col=10000&row=100000');
        $request->assertStatus(500);
    }

    /**
     * @test
     */
    public function CheckMapInstanceTest()
    {
        $request = $this->get('/map/check/map-instance');
        $request->assertStatus(200);
    }

    /**
     * @test
     */
    public function CheckMapInitializationTest()
    {
        $request = $this->get('/map/check/map-init?col=3&row=3');
        $request->assertStatus(200);

        $request = $this->get('/map/check/map-init?col=10000&row=10000');
        $request->assertStatus(500);
    }


    /**
     * @test
     */
    public function CheckFreeClusterTest()
    {
        $request = $this->get('/map/check/map-init?col=3&row=3');
        $request->assertStatus(200);

        $request = $this->get('/map/check/map-init?col=2&row=2');
        $request->assertStatus(500);
    }
}
