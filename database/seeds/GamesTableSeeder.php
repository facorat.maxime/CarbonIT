<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $max = 10;
        for ($i = 1; $i < $max; $i++) {
            DB::table('games')->insert([
                'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur convallis lacus ac purus aliquam sollicitudin. Aenean felis elit, faucibus laoreet lorem quis, viverra molestie leo. Donec et nullam.",
                'title' => "Game #" . $i,
                'img' => '/img/games/game' . $i. '.jpg',
                'platform_id' => random_int(1, 5)
            ]);
        }
    }
}
