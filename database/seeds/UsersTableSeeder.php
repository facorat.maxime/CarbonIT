<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gender = ['M', 'Mrs'];
        for($i=1; $i<30; $i++)
        {
            DB::table('users')->insert([
                'name' => str_random(10),
                'email' => str_random(10). "@example.com",
                'password' => md5(str_random(50)),
                'gender' => array_random($gender),
                'age' => random_int(18, 70),
                'country' => 'FR',
                'street' => str_random(20),
                'zip' => random_int(00000, 99999),
                'phone_number' => random_int(00000, 99999),
                'isAdmin' => false,
                'city' => "Paris",
                "sell_awards" => random_int(0, 10),
                "buyer_awards" => random_int(0, 10),
                "popularity" => random_int(0, 5),
                "about" => str_random(100)
            ]);
        }
    }
}
