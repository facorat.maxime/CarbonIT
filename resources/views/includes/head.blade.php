<meta charset="UTF-8">
<title>CarbonIT</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link href="{{ asset('node_modules/bootstrap/dist/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{ asset('node_modules/bootstrap/dist/css/bootstrap-grid.css')}}" rel="stylesheet">
<link href="{{ asset('node_modules/bootstrap/dist/css/bootstrap-reboot.css')}}" rel="stylesheet">
<link href="{{ asset('node_modules/datatables/media/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/main.css')}}" rel="stylesheet">
