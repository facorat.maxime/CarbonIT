<script type="text/javascript" src="{{ asset('node_modules/jquery/dist/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('node_modules/startbootstrap-sb-admin/js/sb-admin.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('node_modules/datatables/media/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{ asset('node_modules/datatables.net/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/common.js') }}"></script>