@extends('layouts.app')
@section('content')

    <section>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="p-5">
                        <h2 class="display-4">Scoreboard</h2>
                        <table id="scoreTable"
                               class="dataTable display cell-border compact hover order-column row-border stripe">
                            <thead>
                            <tr>
                                <td>Name</td>
                                <td>Score</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($players as $player)
                                <tr>
                                    <td>{{ $player->getName() }}</td>
                                    <td>{{ $player->getScore() }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-5">
                        <h2 class="display-4">Input file</h2>
                        @foreach($input as $line)
                            <div class="row">{{ $line }}</div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-5">
                        <h2 class="display-4">Map input</h2>
                        <div class="row">{!! $map->showMap() !!}</div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-5">
                        <h2 class="display-4">Output file</h2>
                        @foreach($output as $line)
                            <div class="row">{{ $line }}</div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-5">
                        <h2 class="display-4">Map output</h2>
                        {!! $outputMap !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection