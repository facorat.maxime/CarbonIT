<!DOCTYPE html>
<html>
@section('header')
    @include('includes.head')
@endsection
@section('js')
    @include('includes.js')
@endsection
<body>
@section('navbar')
    @include('includes.navbar')
@endsection

@section('topbar')
    @include('includes.topbar')
@endsection

@section('content)
@endsection

@section('footer')
    @include('includes.footer')
@endsection
</body>
</html>