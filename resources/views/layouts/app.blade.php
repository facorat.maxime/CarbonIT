<!DOCTYPE html>
<html>
<head>
    @include('includes.head')
    @include('includes.js')
</head>
<body>
@include('includes.navbar')

@include('includes.topbar')
<div class="container-fluid">
    @yield('content')
</div>
<footer>
    @include('includes.footer')
</footer>

</body>
</html>
