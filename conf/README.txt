For easy launch, execute in linux bash "php artisan serve" and access webpage at 127.0.0.1:8000
PHP_VERSION = 7.2;
LARAVEL_VERSION = 5.6;
NGINX_VERSION = latest;
PHP_UNIT = latest;
